import axios from 'axios'

const namespaced = true;

const state = {
  blogs: [],
  url: "http://demo-api-vue.sanbercloud.com/api/v2",
};

const getters = {
  blogs: state => state.blogs,
  url: state => state.url,
};

const mutations = {
  SET_BLOGS(state, payload) {
    state.blogs = payload
  }
};

const actions = {
  async getAllBlogs({commit}) {
    try {
      const {data} = await axios.get(`${state.url}/blog`)
      console.log(data, "<<<<<<<<< ini datanya")
      commit('SET_BLOGS', data.blogs.data)
    } catch(err) {
      console.log(err)
    }

  }
};

export default { namespaced, state, getters, mutations, actions };